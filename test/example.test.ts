import { assert } from 'chai'

describe('titleA', () => {
    it('should return first charachter of the string', () => {
        assert.equal('Hello'.charAt(0), 'H')
    })
})
